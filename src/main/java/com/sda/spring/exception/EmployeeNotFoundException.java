package com.sda.spring.exception;

public class EmployeeNotFoundException extends RuntimeException {

    //constructor de mesaj de exceptie
    public EmployeeNotFoundException (String message){
        super(message);
    }
}
