package com.sda.spring.config;

import com.sda.spring.components.CustomFaker;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.sda.spring.components")  //ii spun springului unde gaseste componentele de fake
public class AppConfig {
    /**
     * Am comentat Bean-ul deoarece am folosit @ComponentScan si @Component pe clasa CustomFaker
     */

//    @Bean
//    public CustomFaker customFaker(){
//        return new CustomFaker();
//    }


    @Bean
    public ModelMapper modelMapper(){
        return new ModelMapper();
    }

}
